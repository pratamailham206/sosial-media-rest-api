const db = require('../models')
const User = db.users
const bcrypt = require('bcrypt');
const { registerValidation, loginValidation } = require('../validations/user.validation')
var jwt = require('jsonwebtoken');


exports.myaccount = async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        const { password, updateAt, ...other } = user._doc
        res.status(200).send(other)
    } catch (err) {
        res.status(500).send({ 'message': "Account not found" })
    }
}


// Login
exports.login = async (req, res) => {
    const { error } = loginValidation(req.body)
    if (error) return res.status(400).send(error.details[0].message)

    const user = await User.findOne({ email: req.body.email })
    if(!user) return res.status(400).send('Email or password is wrong')

    const validPass = await bcrypt.compare(req.body.password, user.password)
    if(!validPass) return res.status(400).send('Invalid password')

    const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
    
    res.header('auth-token', token).send({
        'token': token
    })
}


// Register
exports.register = async (req, res) => {
    const { error } = registerValidation(req.body)
    if (error) return res.status(400).send({ 'message': error.details[0].message })

    const emailExist = await User.findOne({email: req.body.email})    
    if (emailExist) return res.status(400).send({ 'message': 'Email already Exist' })

    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: hashedPassword
    })

    user.save(user)
    .then((result) => {
        res.send({ user: user._id })
    })
    .catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while create post"
        })
    });
}


// Update Account
exports.updateUser = async (req, res) => {
    if (req.user._id === req.params.id) {
        if(req.body.password) {
            try {
                const salt = await bcrypt.genSalt(10)
                req.body.password = await bcrypt.hash(req.body.password, salt);
            }
            catch (err) {
                res.status(500).json(err)
            }            
        }
        try {
            const user = await User.findByIdAndUpdate(req.params.id, {
                $set: req.body,
            })
            res.status(200).send({
                "message": "Account has been updated"
            })
        } catch (err) {
            return res.status(500).json(err)
        }
    } else {
        return res.status(403).send({
            'message': "You can only update your account"
        });
    }
}


// Delete Account
exports.deleteUser = async (req, res) => {
    if (req.user._id === req.params.id) {
        try {
            const user = await User.findByIdAndDelete(req.params.id)
            res.status(200).send({
                "message": "Account has been deleted"
            })
        } catch (err) {
            return res.status(500).json(err)
        }
    } else {
        return res.status(403).send({
            'message': "You can only delete your account"
        });
    }
}


// View User
exports.viewUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
        const { password, updateAt, ...other } = user._doc
        res.status(200).send(other)
    } catch (err) {
        return res.status(500).json(err)
    }
}


// Follow User
exports.followUser = async (req, res) => {
    if (req.user._id !== req.params.id) {
        try {
            const user = await User.findById(req.params.id)
            const currentUser = await User.findById(req.user._id)

            if(!user.followers.includes(req.user._id)) {
                await user.updateOne({ $push: { followers: req.user._id }})
                await currentUser.updateOne({ $push: { followings: req.params.id }}) 
                res.status(200).send({ 'message': "user has been followed" })

            } else {
                res.status(403).send({ 'message': "you already follow this user" })
            }
        } catch (err) {
            return res.status(500).send({ 'message': 'user not found' })
        }
    } else {
        return res.status(403).send({
            'message': "You can't follow yourself"
        });
    }
}


// Unfollow User
exports.unfollowUser = async (req, res) => {
    if (req.user._id !== req.params.id) {
        try {
            const user = await User.findById(req.params.id)
            const currentUser = await User.findById(req.user._id)

            if(user.followers.includes(req.user._id)) {
                await user.updateOne({ $pull: { followers: req.user._id }})
                await currentUser.updateOne({ $pull: { followings: req.params.id }}) 
                res.status(200).send({ 'message': "user has been unfollowed" })
                
            } else {
                res.status(403).send({ 'message': "you already unfollow this user" })
            }
        } catch (err) {
            return res.status(500).send({ 'message': 'user not found' })
        }
    } else {
        return res.status(403).send({
            'message': "You can't unfollow yourself"
        });
    }
}
