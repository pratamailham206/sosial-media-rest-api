const db = require('../models')
const Post = db.posts
const User = db.users

exports.viewAllPost = async (req, res) => {
    try {
        const posts = await Post.find().populate('comments', 'userId comment')
        res.status(200).send(posts)
    } catch (err) {
        res.status(400).send(err)
    }
}

exports.createPost = async (req, res) => {
    var path = "none";
    if (req.file) {
        path = req.file.path
    }

    const newPost = new Post({
        userId: req.user._id,
        title: req.body.title,
        desc: req.body.desc,
        img: path
    })
    try {
        const savedPost = await newPost.save()
        res.status(200).send(savedPost)
    } catch (err) {
        res.status(500).send(err)
    }
}

exports.viewPost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id).populate('comments', 'userId comment')
        res.status(200).send(post)
    } catch (err) {
        res.status(500).send(err)
    }
}

exports.updatePost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        if (post.userId === req.user._id) {
            await post.updateOne({ $set: req.body });
            res.status(200).send({ 'message': "Your Post updated" })
        } else {
            res.status(403).send({ 'message': 'You can only modify your post'})
        }      
    } catch (err) {
        res.status(500).send({ 'message': "post not found" })
    }
}

exports.likePost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        if(!post.likes.includes(req.user._id)) {
            await post.updateOne({ $push: { likes: req.user._id }})
            res.status(200).send({ 'message': "you liked this post" })
        }
        else {
            await post.updateOne({ $pull: { likes: req.user._id }})
            res.status(200).send({ 'message': "you unliked this post" })
        }
    } catch (err) {
        res.status(500).send({ 'message': "Post not found" })
    }
}

exports.deletePost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        if (post.userId === req.user._id) {
            await post.deleteOne();
            res.status(200).send({ 'message': "Your Post deleted" })
        } else {
            res.status(403).send({ 'message': 'You can only delete your post'})
        }      
    } catch (err) {
        res.status(500).send({ 'message': "post not found" })
    }
}

exports.myTimeline = async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        const userPosts = await Post.find({ userId: user._id })
        const followingPosts = await Promise.all(
            user.followings.map((followingId) => {
                return Post.find({ userId: followingId })
            })
        )
        res.send(userPosts.concat(...followingPosts))
    } catch (err) {
        res.status(500).json(err)
    }
}

exports.myPost = async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        const userPosts = await Post.find({ userId: user._id }).populate('comments', 'userId comment')
        res.send(userPosts)
    } catch (err) {
        res.status(500).json(err)
    }
}

exports.otherPost = async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
        const userPosts = await Post.find({ userId: user._id }).populate('comments', 'userId comment')
        res.send(userPosts)
    } catch (err) {
        res.status(500).json(err)
    }
}