const db = require('../models')
const Post = db.posts
const Comment = db.comments

exports.addComment = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        const comment = new Comment({
            postId: post._id,
            userId: req.user._id,
            comment: req.body.comment
        })
        const saveComment = await comment.save()
        await post.updateOne({ $push: { comments: saveComment._id }}) 

        res.status(200).send({ 'message': "Comment added" })
    } catch (err) {
        res.status(500).send(err)
    }
}