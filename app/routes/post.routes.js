const posts = require('../controllers/post.controller')
const comments = require('../controllers/comment.controller')
const router = require('express').Router()
const verify = require('./verifyToken')
const { uploadImg } = require('../middlewares/upload')

router.get('/', verify, posts.viewAllPost)
router.post('/', verify, uploadImg, posts.createPost)
router.get('/detail/:id', verify, posts.viewPost)
router.put('/update/:id', verify, posts.updatePost)
router.delete('/delete/:id', verify, posts.deletePost)
router.put('/:id/like', verify, posts.likePost)
router.get('/timeline', verify, posts.myTimeline)
router.get('/myPost/', verify, posts.myPost)
router.get('/otherpost/:id', verify, posts.otherPost)
router.post('/addcomment/:id', verify, comments.addComment)

module.exports = router