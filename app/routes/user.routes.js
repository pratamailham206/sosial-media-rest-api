const users = require('../controllers/user.controller')
const router = require('express').Router()
const verify = require('./verifyToken')

router.post('/login', users.login)
router.post('/register', users.register)
router.get('/myaccount', verify, users.myaccount)
router.put('/update/:id', verify, users.updateUser)
router.delete('/delete/:id', verify, users.deleteUser)
router.get('/:id', verify, users.viewUser)
router.put('/:id/follow', verify, users.followUser)
router.put('/:id/unfollow', verify, users.unfollowUser)

module.exports = router