module.exports = (mongoose) => {
    const schema = mongoose.Schema(
        {
            userId: {
                type: String,
                required: true,
            },
            title: {
                type: String,
                required: true,
                max: 255
            },
            desc: {
                type: String,
                max: 500,
            },
            img: {
                type: String
            },
            likes: {
                type: Array,
                default: [],
            },
            comments: [
                { type: mongoose.Schema.Types.ObjectId, ref: 'comments' }
            ]
        },
        { timestamps: true }
    )

    schema.method("toJSON", function() {
        const {__v, _id, ...object} = this.toObject()
        object.id = _id
        return object
    })

    const Post = mongoose.model("posts", schema)
    return Post
}