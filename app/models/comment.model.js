module.exports = (mongoose) => {
    const schema = mongoose.Schema({
        postId: {
            type: String,
            required: true,
        },
        userId: {
            type: String,
            required: true,
        },
        comment: {
            type: String,
            max: 255,
            default: ""
        }
    }, { timestamps: true })

    const Comment = mongoose.model("comments", schema)
    return Comment
}