const mongoose = require('mongoose')

mongoose.Promise = global.Promise


const db = {}
db.mongoose = mongoose
db.posts = require('./post.model')(mongoose)
db.users = require('./user.model')(mongoose)
db.comments = require('./comment.model')(mongoose)
module.exports = db