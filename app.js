const express = require('express')
const cors = require('cors')
const dotenv = require('dotenv');
const app = express()
const db = require('./app/models');
const helmet = require("helmet");
const morgan = require('morgan')
const userRoute = require('./app/routes/user.routes')
const postRoutes = require('./app/routes/post.routes');

dotenv.config();

// DB Connect
db.mongoose
    .connect(process.env.DB_CONFIG, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }).then(() => {
        console.log("Database connected")
    }).catch((err) => {
        console.log("Cannot connect to the database")
    })


app.get('/', (req, res) => {
    res.json({
        message: "welcome to latihan_express"
    })
})


// Middleware
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(helmet())
app.use(morgan('common'))
app.use('/uploads', express.static('./uploads'));


// Route
app.use('/api/posts', postRoutes)
app.use('/api/users', userRoute)


// Initialize
app.listen(process.env.PORT, () => {
    console.log(`Server is running on http://localhost:${process.env.PORT}`)
})